import firebase_admin
from firebase_admin import messaging
import psycopg2
from firebase_admin import credentials, db
from firebase import firebase
import requests
from requests.auth import HTTPBasicAuth
import humanize
import datetime

firebase = firebase.FirebaseApplication('https://aliposter-default-rtdb.firebaseio.com', None)
cred = credentials.Certificate("key.json")
firebase_admin.initialize_app(cred, {'databaseURL': 'https://aliposter-default-rtdb.firebaseio.com'})


def send_message(token):
    message = messaging.Message(
        data={
            "to": "Заказ принят"
        },
        token=token
    )
    messaging.send(message)



def send_sms(to, text, id):
    try:

        sms_id = f"chickenexpress{id}"
        url = 'http://91.204.239.44/broker-api/send'
        data = {
            "messages": [
                {
                    "recipient": to,
                    "message-id": sms_id,
                    "sms": {
                        "originator": "3700",
                        "content": {
                            "text": str(text)
                        }
                    }
                }
            ]
        }
        headers = {'Content-Type': 'application/json'}
        r = requests.post(url, json=data,
                          headers=headers, auth=HTTPBasicAuth('chickenexpressuz', 'T3n2iCFt96'))
        return r
    except Exception as ex:
        print(ex)
        return ex


def lister(event):
    try:
        data = event.data
        if event.path != '/':
            path = event.path
            if path.count('/') == 1:
                total = humanize.intcomma(data['total'])
                try:
                    products = data['products']
                except:
                    products = ''
                text = f"Nomer zakaza {data['unique_id']} Summa: {total} + dostavka {products}"
                try:
                    mydb = psycopg2.connect(user="postgres",
                                            password="123",
                                            host="127.0.0.1",
                                            port="5432",
                                            database="chefdb")
                    db_cursor = mydb.cursor()
                    insert_query = "INSERT INTO sms (id, msdsn, text, date ) VALUES ({}, '{}', '{}', '{}') RETURNING id;".format(
                        data['unique_id'], data['client_phone'], str(text), datetime.datetime.now().date())
                    db_cursor.execute(insert_query)
                    pk = db_cursor.fetchone()[0]
                    mydb.commit()
                except Exception as e:
                    pk = 0
                    print(e)
                # send_sms(
                #     data['client_phone'].replace('+', '').replace('(', '').replace(')', '').replace(' ', '').replace(
                #         '-', ''), text, pk)
                workers = list(firebase.get('/workers/', None))
                for worker in workers:
                    wk = firebase.get('/workers/', worker)
                    try:
                        send_message(wk['token'])
                    except Exception as e:
                        print(e)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    db.reference('order').listen(lister)
